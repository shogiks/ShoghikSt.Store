﻿using Store.Helper;
using Store.Models;
using StoreRepository.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using CaptchaMvc.HtmlHelpers;


namespace Store.Controllers
{
    public class AccountController : Controller
    {
        private StoreDBEntities _context = new StoreDBEntities();

        // GET: Account
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Registration()
        {
            return View();
        }

        public ActionResult Home()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Registration(StoreRegViewModel model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var user = model.GetEntity();
                    _context.Users.Add(user);
                    _context.SaveChanges();
                    Response.SetAuthCookie(user);
                    //Session["UserId"] = user.UserId.ToString();
                    //Session["UserName"] = user.UserName.ToString();
                    
                    model.UserName = "";
                    ViewBag.Message = user.Name + " " + user.Surname + " successfully regisstered";    
                    ModelState.Clear();   
                   // return RedirectToAction("Home", "Account");

                }
                return View(model);
            }
            catch (Exception)
            {
                return null;
            }
         }
        
        public ActionResult Login()
        {
            return View(new StoreLoginViewModel());
        }

        [HttpPost]
        public ActionResult Login(StoreLoginViewModel model)
        {
            
            var user = _context.Users.FirstOrDefault(x => x.UserName == model.UserName && x.Password == model.Password);
            if (user != null && this.IsCaptchaValid("Captcha is not valid"))
            {
                Session["UserId"] = user.UserId.ToString();
                Session["UserName"] = user.UserName.ToString();
                return RedirectToAction("Home");
            }
            else if (user == null)
            {
                ModelState.AddModelError("", "UserName or Password id wrong. ");
            }
            else
            {
                ModelState.AddModelError("", "Captcha is not valid. ");
            }
            
            return View();
        }

        public ActionResult LogOut()
        {
            Session.Clear();
            FormsAuthentication.SignOut();
            return RedirectToAction("Login");
        }
    }
}