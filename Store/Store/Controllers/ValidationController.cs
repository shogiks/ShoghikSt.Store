﻿using StoreRepository.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Store.Controllers
{
    public class ValidationController : Controller
    {
       private StoreDBEntities _context = new StoreDBEntities();
        // GET: Validation
        public ActionResult Index()
        {
            return View();
        }

        
        public JsonResult IsUserNameExist(string userName)
        {
            
            bool isExist = _context.Users.FirstOrDefault(u => u.UserName.ToLower().Equals(userName.ToLower())) != null;
            return Json(!isExist, JsonRequestBehavior.AllowGet);
        }
    }
}