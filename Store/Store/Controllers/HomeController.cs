﻿using Store.Models;
using StoreRepository.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Store.Controllers
{
    public class HomeController : Controller
    {
        private StoreDBEntities _context = new StoreDBEntities();
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public ActionResult Category()
        {
            var listCategories = new List<StoreCategoryViewModel>();
            foreach (var item in _context.Categories)
            {
                listCategories.Add(new StoreCategoryViewModel
                {
                    CategoryId = item.CategoryId,
                    CategoryName = item.CategoryName,
                    ParentId = item.ParentId
                });
            }
            return View(listCategories);
        }

        public ActionResult Brand()
        {
            var listBrands = new List<StoreBrandViewModel>();
            foreach (var item in _context.Brands)
            {
                listBrands.Add(new StoreBrandViewModel
                {
                    BrandId = item.BrandId,
                    BrandName = item.BrandName,
                  
                });
            }
            return View(listBrands);
        }

        public ActionResult Link()
        {
            return View();
        }

        public ActionResult TextArea()
        {
            return View();
        }
    }
}