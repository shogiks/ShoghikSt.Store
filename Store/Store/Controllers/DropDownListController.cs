﻿using Store.Models;
using StoreRepository.Entities;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Store.Controllers
{
    public class DropDownListController : Controller
    {
        private StoreDBEntities _context = new StoreDBEntities();
        // GET: DropDownList
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public ActionResult Kendo()
        {

            if (Request != null)
            {
                HttpPostedFileBase file = Request.Files["UploadedFile"];

                if ((file != null) && (file.ContentLength > 0) && !string.IsNullOrEmpty(file.FileName))
                {
                    string fileName = file.FileName;
                    string fileContentType = file.ContentType;
                    byte[] fileBytes = new byte[file.ContentLength];
                    file.InputStream.Read(fileBytes, 0, Convert.ToInt32(file.ContentLength));
                }
            }

            return View("Kendo");


          
        }

        public void Upload()  //Here just store 'Image' in a folder in Project Directory 
                              //  name 'UplodedFiles'
        {
            
            foreach (string file in Request.Files)
            {
                var postedFile = Request.Files[file];
                postedFile.SaveAs(Server.MapPath("~/Uploads/") + postedFile.FileName );
            }

         //   return View()
        }

        //public ActionResult List() //I retrive Images List by using this Controller
        //{
        //    var uploadedFiles = new List<Picture>();

        //    var files = Directory.GetFiles(Server.MapPath("~/UploadedFiles"));

        //    foreach (var file in files)
        //    {
        //        var fileInfo = new FileInfo(file);

        //        var picture = new Picture() { Name = Path.GetFileName(file) };
        //        picture.Size = fileInfo.Length;

        //        picture.Path = ("~/UploadedFiles/") + Path.GetFileName(file);
        //        uploadedFiles.Add(picture);
        //    }

        //    return View(uploadedFiles);
        //}



    }
}