﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using StoreRepository.Entities;
using Store.Models;
using System.IO;

namespace Store.Controllers
{
    public class ProductController : Controller
    {
        private StoreDBEntities _context = new StoreDBEntities();
        // GET: Item
        public ActionResult Index()
        {
            return View();
        }
        //[HttpPost]
        public ActionResult RegProduct(int? id)
        {
            var ProductID = Convert.ToInt64(id);

            StoreProductViewModel model = new Models.StoreProductViewModel();
           
            if (ProductID == 0)
            {
                // add product...
                model.product = new Product();
               // Upload(model);
            }
            else
            {
                // update mode
                model.product = _context.Products.Where(p => p.ProductId == ProductID).FirstOrDefault();
            }

            model.CategoryList = _context.Categories.OrderBy(p => p.CategoryName).Select(p => new SelectListItem { Value = p.CategoryId.ToString(), Text = p.CategoryName }).ToList();
            model.BrandList = _context.Brands.OrderBy(p => p.BrandName).Select(p=> new SelectListItem { Value = p.BrandId.ToString(),Text = p.BrandName}).ToList();
            
            return View(model);
        }
        public JsonResult Upload(StoreProductViewModel model)
        {
            string path = model.product.MainImageName;
            string fileName = Path.GetFileNameWithoutExtension(path);
            string ext = Path.GetExtension(path);

            fileName = fileName + DateTime.Now.ToString("yymmssfff") + ext;
            path = "~/Uploads/" + fileName;

            fileName = Path.Combine(Server.MapPath("~/Uploads/"), fileName);

            model.ImageFile.SaveAs(fileName);
            _context.Products.Add(model.product);
            _context.SaveChanges();
            
            //_context.Products.Add(model);
            return Json(fileName, JsonRequestBehavior.AllowGet);
                         
          }

        public ActionResult DisplayingImage(string imgName)
        {
            var img = _context.Products.SingleOrDefault(x => x.MainImageName == imgName);
            return File(img.MainImageName, "jpg");
        }

    }
   
}