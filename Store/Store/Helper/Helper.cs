﻿using StoreRepository.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;

namespace Store.Helper
{
    static public class Helper
    {
        public static bool SetAuthCookie(this HttpResponseBase response, User user)
        {
            FormsAuthentication.SetAuthCookie(user.UserName, false);

            var authTicket = new FormsAuthenticationTicket(1, user.UserName, DateTime.Now, DateTime.Now.AddMinutes(20), false,user.Name);
            string encryptedTicket = FormsAuthentication.Encrypt(authTicket);
            var authCookie = new HttpCookie(FormsAuthentication.FormsCookieName, encryptedTicket);
            try
            {
                response.Cookies.Add(authCookie);
                return true;
            }
            catch (Exception)
            {
                return false;
            }

        }
    }
}