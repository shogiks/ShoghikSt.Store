﻿using StoreRepository.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Store.Models
{
    public class StoreBrandViewModel
    {
        public int BrandId { get; set; }
        public string BrandName { get; set; }
    }

  
 }