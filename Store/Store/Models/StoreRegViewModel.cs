﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using StoreRepository.Entities;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace Store.Models
{
    

    public class StoreRegViewModel
    {

        // [VerifyUserName(ErrorMessage = "This fiels is required!!!")]
        [Required]
        [Display(Name = "First name")]
        public string FirstName { get; set; }

        [Required]
        [Display(Name = "Last name")]
        public string LastName { get; set; }

        [Required]
        [Display(Name = "User name")]
        [Remote ("IsUserNameExist","Validation", ErrorMessage = "UserName already exist")]
        public string UserName { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }


        [Display(Name = "Confirm Password")]
        [System.Web.Mvc.Compare("Password", ErrorMessage = "The password and confirmation password do not match .")]
        public string ConfirmPassword { get; set; }

        [Required]
        [EmailAddress]
        [Display(Name = "E-mail")]
        public string Email { get; set; }


        //TODO
        //MOVE GetEntity to Reposiory

        public User GetEntity()
        {
            var entity = new User
            {
                Name = FirstName,
                Surname = LastName,
                UserName = UserName,
                Password = Password,
                Email = Email,
                Date = DateTime.Now
            };

            return entity;
        }
    }


}