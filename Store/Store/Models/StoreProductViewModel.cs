﻿using Store.Models;
using StoreRepository.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Store.Models
{
    public class StoreProductViewModel
    {
        public Product product;

        public IEnumerable<SelectListItem> CategoryList;
        public IEnumerable<SelectListItem> BrandList;

        [Display(Name ="Upload Main Image")]
        public HttpPostedFileBase ImageFile { get; set; }

        //string Title { get; set; }
        //string Code { get; set; }
        //int Price { get; set; }
        //int BrandId { get; set; }
        //int CategoryId { get; set; }
        //string Description { get; set; }

        //string Discount { get; set; }
        //string MainImage { get; set; }

        //int UserId { get; set; }
        //DateTime DestroyDate { get; set; }

        //Random rnd = new Random(); 

        //public Product ProductGetEntity()
        //{
        //    var entity = new Product
        //    {
        //        Date = DateTime.Now,
        //        Title = Title,
        //        Code = rnd.Next().ToString(),
        //        Price = Price,
        //        BrandId = BrandId,
        //        CategoryId = CategoryId,
        //        Description = Description,
        //        Discount = Discount,
        //        MainImageName = MainImage,
        //        UserId = UserId,
        //        DestroyDate = DestroyDate
        //    };


        //    return entity;
        //}
    }
}