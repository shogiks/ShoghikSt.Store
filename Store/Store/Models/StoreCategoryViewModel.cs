﻿using StoreRepository.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Store.Models
{
    public class StoreCategoryViewModel
    {
        public int CategoryId { get; set; }
        public string CategoryName { get; set; }
        public int ParentId { get; set; }

        IEnumerable<SelectListItem> CategoryList { get; set; }
              
    }
}